$LOAD_PATH << File.expand_path('..', __dir__)

require 'middleman-core'
require 'middleman-core/rack'

require 'middleman-autoprefixer'
require 'middleman-blog'
require 'middleman-syntax'

require 'lib/bamboohr'
require 'lib/changelog'
require 'lib/release_posts'
require 'lib/gitlab/file_cache'
require 'lib/homepage'
require 'lib/redirect'
require 'lib/code_owners'

require 'spec/support/capybara'
require 'webmock/rspec'

RSpec.configure do |config|
  config.define_derived_metadata(file_path: %r{/spec/features}) do |metadata|
    metadata[:feature] = true
    metadata[:type] = :feature
  end

  # Capybara js driver uses localhost/__identify__ path that we needs to allow
  WebMock.disable_net_connect!(allow_localhost: true)

  # Really slow, only do it once, and only when needed
  config.before(:suite) do
    # Skip if there are no examples with `feature` tag
    next if config.world.example_groups.none? { |example| example.metadata[:feature] } || config.exclusion_filter[:feature]

    root = File.expand_path(File.join(File.dirname(__FILE__), ".."))
    Middleman::Logger.singleton(Logger::WARN, false)

    if ENV['CAPYBARA_LOCALHOST'].try(:match?, /^(true|yes|1)$/i)
      # Run tests against already-running localhost, instead of
      # (very, very slowly) starting Middleman under the default driver
      Capybara.current_driver = :selenium
      Capybara.app_host = 'http://localhost:4567'
      Capybara.run_server = false
    else
      # TODO: Disabling running of rspec feature specs without already-running local server. This needs
      #       to be updated to reflect the new monorepo structure, but we haven't done that because we
      #       haven't been running feature specs in CI for a while, they are too flaky to be useful.
      raise "Running Capybara integration specs is no longer supported. If you just want to run the unit specs, try running `bundle exec rspec --tag ~@feature` instead."
      # rubocop:disable Lint/UnreachableCode
      middleman_app = ::Middleman::Application.new do
        set :root, root
        set :environment, :test
      end
      Capybara.app = Middleman::Rack.new(middleman_app).to_app
      # rubocop:enable Lint/UnreachableCode
    end

    Capybara.save_path = "tmp/capybara"
    Capybara::Screenshot.instance_variable_set(:@capybara_root, File.expand_path(Capybara.save_path, root))
  end

  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.order = :random
  Kernel.srand config.seed

  config.filter_run focus: true
  config.run_all_when_everything_filtered = true
end
