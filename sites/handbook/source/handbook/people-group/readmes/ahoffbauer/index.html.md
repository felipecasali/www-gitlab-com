---
layout: markdown_page
title: "April Hoffbauer's README"
description: "This page is April Hoffbauer's readme and is intended to be helpful when interacting with her."
---

### My Job
My job is to set expectations, communicate context, remove obstacles before they happen and build processes. In general; to make your (work) life better, help you deal with the volume, the speed we operate at and to reduce stress.

I have the privilege of helping you develop. I do this by setting goals, working on a development plan that is applicable to every day, providing feedback and acting as a sounding board and cheerleader.

### My Availability
I love working async - but I also talk through things easier than writing my thoughts out. Many times if you ask me a question via Slack my response will be /zoom. If you can't zoom just let me know and I'll try to answer through text.

If you need me you have to tell me - I'll hop online and help if I can. I'm here to support you and your role!

I snooze my Slack notifications when I'm not available. If you think of something at 2 a.m. my time it's perfectly fine to send me a message - I may be up anyway. I assume you will manage your notifications as well.

On my calendar, I block out my day because of interviews scheduled through Calendly. If I have a DNB or Busy on my calendar it's best to ping me on Slack before scheduling something - otherwise blocks you can schedule over if you can't find anything else that works for the both of us.

#### Meetings
Because of interviews most of my meetings fall under the 25 min rule or 45-50 minute rule. I usually hop the call a few mins early because if I don't join when my timer goes off I'll get busy and forget to join until after the meeting should have started. I love to chat with whoever joins early about the most random things!

The only "mandatory" meeting is our 1:1. For the most part, every other meeting on your calendar is optional - attend if you can. This goes without saying, but that statement does not apply to scheduled interviews. I also try to record as many meetings as possible and upload them to our team drive.

### My Assumptions
- You care about the candidate experience.
- You'll get your work done when it works best for you.
- You care about our relationship with the hiring teams.
- You know the team priorities and how they fit in with your personal priorities.
- You will ask for help if you need it.
- You'll dogfood as much as possible and seek ways to do so more.
- You feel safe pushing back.
- You'll create a MR if there is something we could do better or if a process is out of date.

### Strengths/Weaknesses
**I'm never satisfied.** I’ll celebrate your success/wins but I’ll always ask, what’s next? Literally, in the next sentence, I'll ask what's next. It's perfectly fine if you want to sit in your success for a bit, but I'll be on to the next thing already.

**Tell me if you need to vent. I'm a fixer.** If you need to vent about something I'll be a great listener, but you need to tell me you are just venting. Otherwise, I'll ask for a solution to the problem, and dive into making things better.

**I delete things from my brain.** I check things off my to-do list or mentally check them off as done and I forget them a moment later. This doesn't mean I don't care about things, or it wasn't important. My brain is always going a mile a minute - I'm very strategic - so I can't afford to think about things that are done in my mind. If we are talking about things from a historic perspective I may need help remembering the situation. 

**I reprioritize, a lot.** You need to tell me if something is time-sensitive and give me a deadline, otherwise, I'll gauge myself how much of a priority the situation is and you may be waiting on me longer than you'd like. Generally speaking, if it takes 2 minutes or less to do something I'll do it right away. If it requires more time or more thinking I'll add it into my to-do list.

**I operate with a high degree of trust.** You were hired because of your ability to do your job.
You are free to prioritize the tasks that you feel are the most impactful. With this trust comes a level of hands-off, you may feel like I don't care what you're working on. This couldn't be further from the truth, I just need you to tell me.

**I try to protect you from being overworked.** Every 1:1 I'll ask you about your workload. If you are able to take on more let me know. There is always plenty of work to be done. I'll make sure it gets done eventually, even if it means doing something myself.

### Your Development
One of the most important aspects of my role is to help you grow. If you want to master your current role, grow into the next role, be the best leader you can be - I want to help. You may be a people leader, or you may lead as an individual contributor - your influence on those around you determine your immediate and future success.

I promise to help you find projects that require you to learn, cheer you on and broadcast your successes, and will play devil's advocate when necessary. I will do my best to lend strength to your voice, and help your ideas find their way into results.

### Behaviours I Value
- Initiative and willingness to help out - you see a problem, formulate a solution and act on it, asking for input if necessary.
- Consistency and follow-through - you do what you say you'll do most (if not all) of the time.
- Vulnerability - if you don't know, you ask, and in so doing, create an atmosphere where it's okay to not know and ask.
- Don't claim to be an expert at everything - if I ask you about something and it is being handled by someone else on the team direct me to ask them, don't pretend to know what's going on if you don't.
- Don't play the blame game - when things go wrong human nature tends to make us point the finger at someone else. Take responsibility for the situation, tell me how you are going to fix things, address the take-away so it doesn't happen again and then move on.

#### Feedback For Me
Like you, I rely on feedback to improve. If you feel there is something I could do better, please tell me. I'm very transparent, direct and open to feedback.

If you don't feel comfortable addressing something with me directly, please tell my manager. No for real - tell them!

### 1:1 Meetings
This is your 25-minute meeting and your time. You set the agenda and determine what we talk about outside of the status update report. If we get through all of the agenda items expect me to try and get to know you better. I dislike skipping our time together if at all possible. You have edit access to this recurring meeting, if you need to reschedule please do so.

My 1:1s do have some structure and require a bit of prep time, please come prepared. Every 1:1 has a Google Doc agenda accessible to the direct report and myself only. There is a template at the bottom of the 1:1 doc that will include the links to the KPIs, OKRs and Performance Indicators.

The status update consists of
- Weekly updates on your OKRs, KPIs and Performance Indicators (PI) according to your role.
- Where can we improve on each metric and why?
- Celebrate when things are going good and we are meeting our goals!
- Month-end retrospective on your OKRs, KPIs and PI when we roll over into a new month.

I like hearing about situations in your week because
- I always learn something.
- I get data points that help me make global process improvements.
- I get a picture of what things you're great at.
- I begin to understand which things you struggle with.

If you're looking for feedback and I'm not offering it, it means I think that you're doing just fine. If you're proud of something, please bring it up! If you're not getting feedback and you want some, please ask for feedback in a specific area.

Don't save urgent matters for a 1:1. Personally I like to talk about things in the moment and don't save many things to discuss until our next meeting. You may not feel this way or may need more time to reflect on things. That is ok, it's just not me. If it's a quick note/feedback I'll probably just tell you via Slack.


Do you know something about me that belongs here? Submit an MR to this page.
