---
layout: handbook-page-toc
title: "Account Planning"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview 

An Account Plan is a single document that contains important details about a new prospect or existing customer, including information about their decision-making process, the companies you’re competing with, and your overall strategy to win them over (and retain them). 

Think about account planning as if you were the CEO of your own territory. If you were a CEO, what information would you need to give banks, investors, or employees to demonstrate that you have a viable business plan that you can execute? An account plan is a business plan that helps you analyze and execute on your business.  

## Account Planning vs. Success Planning 

While the account plan focuses on the account team's strategy to win and retain customers, success planning clearly documents what and how GitLab will deliver value throughout the customer lifecycle to help the customer optimize their return on investment.

### Why Account Planning?

Account Planning helps you focus on elevating opportunity-driven conversations into value based conversations that focus on our [value drivers](/handbook/sales/command-of-the-message/#customer-value-drivers).  Good account plans can help you focus on what matters and lead to bigger deals and better renewals, and ensures more predicitable growth.

An account plan can help:
* see what is going on across the business and determine what opportunities provide the biggest return on your team’s efforts
* determine if your team has the resources to land this account
* set your goals based on the opportunities and your resources to land the account
* turn your goals into strategic objectives
* determine what engagement approaches will you use to achieve the strategic objectives
* assess the risks of failure and layout contingency plans


## Collaborate with the Account Team 

Account Plans should be toughtful, collaborative, and ongoing efforts with the entire account team, including Solutions Architects, Technical Account Managers, and Sales Leadership. 

## Account Plans are Living Documents

It's important to share progress with account team as things change. The SALSA/ TAM team should  be kept up to date so they can continue working on new opportunities and in turn sharing the progress with anyone else they think should be aware.

A customer’s business and strategies will change, to stay up to date, share the account plan to the account team regularly to help keep a fresh understanding of the customer's needs. 

It's recommended SALs to use QBRs and/or other recurring meetings such as SALSA/TAM Calls) to review steps achieved thus far and set next steps or new objectives as needed. 

## Components of an Account Plan 
* Account Details 
* People Maps, including roles and reporting structure 
* Whitespace Mapping, including their current stages (whether GitLab, competitor, or status quo) and which stages should be targeted
* Action Plan

## Building the Account Plan 

### Step 1: Account Tiering 

Not all accounts need an account plan, and the first step is determining which accounts need one. 


### Step 2: Understand the Customer’s Motivation

Always start with the Customer's "Why?", and get clarity on the customer’s strategies, goals, and objectives.

## Step 3: Start Building

Account Planning needs a long term ‘vision’ for the account that both parties agree to work towards. A good place to start is with a 5 year plan. You can start yb asking the customer: “Where do you want to be with GitLab in 5 years?” Take that answer and make it the 1st point of the plan, then figure out a workback plan. 

### Samples
5 year vision:
**The Customer says...“In 5 years, the business will be …”**
* “To get there, the business will need to…”
* “Software development will play the role of…”
* “Our software teams will need to be able to…”
* “Our use of GitLab to support this will be…”
* “To get to that stage with GitLab, we will need to do…”
* “How we do that, is something … will need to own on our side”

Build out a plan of required capabilities, prioritise them, and then order them.
Somes examples 
* “In 5 years, the business will be operating in 6 new countries, with 2 new product lines.  
* To get there the business will need to hire new talent, we will need to build the products, we will need to open 6 new regional entities.  We will also need to ensure that we are not over-extending the core profit making part of the business.  
* Software development’s role will be: 1) the products will be either fully or partially digital; 2) to launch the new product lines, in the new markets we wish to enter, will require new technical expertise in… AI/robotics/VR etc; 3) dev will need to ensure that the core cash-cow products at least keep their current margins while reducing cost.  





