---
layout: handbook-page-toc
title: Commercial SA Engagement Model
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

[**SA Practices**](/handbook/customer-success/solutions-architects/sa-practices) - [**Sales Plays**](/handbook/customer-success/solutions-architects/sales-plays) - [**Tools and Resources**](/handbook/customer-success/solutions-architects/tools-and-resources) - [**Career Development**](/handbook/customer-success/solutions-architects/career-development) - [**Demonstration**](/handbook/customer-success/solutions-architects/demonstrations) - [**Processes**](/handbook/customer-success/solutions-architects/processes)



## Commercial Solutions Architecture Engagement Model

Wherever possible, let's bring synchronicity back to our sales and customer-facing team members and relationships.

Anytime we use text-based communication, we might miss a chance to connect with someone - which can be critical to our personal and business relationships. While asynchronous communication is important, it can sometimes hamper our ability to clearly communicate. In contrast, a conversation allows us to establish a relationship with the other person and perhaps communicate clearer.

The Commercial SA Engagement Model intends to foster collaboration and influence and even greater iteration amongst ourselves and customers.



#### SA Engagement Considerations

- All Solutions Architect (b.k.a SA) requests are submitted via [Americas](https://gitlab.com/gitlab-com/customer-success/sa-triage-boards/commercial-triage/-/boards/1006966#) and [EMEA](https://gitlab.com/gitlab-com/customer-success/sa-triage-boards/emea-commercial-triage/-/boards/1371455)  triage boards

- SA notes and activities are logged within Salesforce via Troops.ai (review the [SA Activity Capture page](https://about.gitlab.com/handbook/customer-success/solutions-architects/processes/activity-capture/)).

- Deal qualification is simply any deal that **is piped at $10k and higher** focused not only on engagement value but also customer potential for consideration of Solutions Architect assignment.

- All Salesforce linked active opportunities should have [MEDDPPICC](https://about.gitlab.com/handbook/sales/meddppicc/) (and [required 7 methods applied](https://about.gitlab.com/handbook/sales/commercial/#required-7))

- Compelling events are clearly defined or the issue (i.e. triage request) has a clear statement indicating why that information isn’t available yet and how the SA can help obtain it.



#### Meeting Expectations

- All meetings should be planned with clear desired outcomes available to the SA
  - Why does the prospect want to meet with us?
  - What are our meeting objectives/goals?
  - Agenda and list of attendees should be provided in advance; <u>failure to provide this information could delay in the scheduling or declination of a meeting request.</u>
- SA activities include:
  - Discovery calls
  - Demonstrations
  - [Proof-of-Value engagements (b.k.a POVs)](https://about.gitlab.com/handbook/sales/POV/#pov-best-practices) 
    - POV's  should  be positioned we're engagements are $25k or higher
    - Pro tip: All POV/evaluation scope should be negotiated and vetted, so we understand “why”. Let’s not rush to answer.  We should validate our prospects’/customers’ needs and know what they’re asking for. This will help us provide a solution by establishing success criteria to evaluate and confirm Gitlab as their best path forward. "Production" and "Pre-production" level POVs/evals should be discouraged or provided as paid engagement option as part of a license purchase.



*Please Note: Exceptions can and should be made via the issue commenting, mentioning both the ASM (aka Area Sales Manager as well as the Commercial SA Leader).*



#### Opportunities below $10K

In the spirit of supporting the sales floor and deals below $10k we’d like to continue to use Slack offering same day SLA’s for initial response and validation of the ask with mutually agreed upon delivery on formal answers to the question(s) posed.

**Now Available:** **[SA Office Hours](https://gitlab.com/gitlab-com/customer-success/solutions-architecture-leaders/sa-initiatives/-/issues/33)** 

To help with deals below $10k, we have introduced office hours as a forum that allows AE's to ask questions of the SA team in a one-to-many fashion that may help accelerate a smaller engagement, increase their knowledge of Gitlab and other industry related technologies.



*Please Note: If an exception needs to be made please copy both the ASM and Commercial SA Leader should a response be needed quicker though we can’t guarantee that this will be fulfilled.*



#### Other Considerations



##### Team meetings

- We hold our team meetings on Mondays (Americas) at 11am EST and Tuesdays (EMEA) 9.30am EST for an hour - we expect that unless we’re being asked to participate in a critical customer call that our folks attend our meetings. 

- Try to be respectufl of our scheduled 1:1's meetings, though they can be more easily rescheduled in favor of customer engagement.


##### Meeting Follow up/Research

- Solutions Architects need time to provide follow up with information in as near real-time as possible. We aim to minimize scheduling “back-to-back” meetings as this can compromise our ability to provide our best possible response for the customers/prospects we're supporting.
