---
layout: solutions
title: GitLab on VMware Tanzu
description: "GitLab on VMware Tanzu."
canonical_path: "/solutions/vmware-tanzu/"
suppress_header: true
---

***
{:.header-start}

![VMware Tanzu logo](/images/logos/TZLG-VMwareTanzu-FULLCOLOR-604x135-50fbb10.png)
{:.header-right}

# GitLab on VMware Tanzu
{:.header-left}

Paired with the flexibility and portability of VMware Tanzu, GitLab’s suite of features will further enable development and operations teams to work together, and work smarter. 
{:.header-left}

[Talk to an expert](/sales/){: .btn .cta-btn .orange}
{:.header-left}

***
{:.header-end}

***

## Joint VMware and GitLab Benefits

Together, VMware Tanzu and GitLab enable companies with a digital transformation to meet the challenges of planning, creating, securing and building the software that differentiates them in the marketplace.

For organizations looking to improve operational efficiencies,GitLab offers a consistent, scalable interface between different teams in order to deliver applications faster onto VMware Tanzu platform.

To build revenue generating products faster, while managing priorities, security and compliance risks, GitLab offers seamless collaboration across Dev, Ops and compliance teams for continuous innovation on VMware Tanzu platform.

* Projects delivered on-time and budget
    * Eliminate bottlenecks for agility, faster DevOps lifecycle, reduce re-work, reduce unpredictable spend.
* Increase team productivity and velocity.
    * Attract, retain, and enable top talent, move engineers from integrations and maintenance to innovation, happy developers.
* Increase market share and revenue
    * Faster time to market, disrupt the competition; increased innovation; more expansive product roadmap.
* Increase customer satisfaction
    * Decrease security exposure, cleaner, and easier audits reduce disruptions.
{:.list-benefits}

Instead of using multiple tools during software development lifecycle, GitLab offers a single tool to develop Cloud Native applications for the VMware Tanzu platform.

Build, secure, deploy and migrate applications to TAS, TKG, or vSphere via complete DevSecOps self-service model with GitLab and VMware Tanzu

***

## Existing Integrations

* ![ VMware Tanzu logo](/images/logos/TZLG-VMwareTanzu-icon.png) VMware Tanzu Solutions Hub
    * GitLab is a featured DevOps Tool in the [VMware Tanzu Marketplace](https://tanzu.vmware.com/solutions-hub/devops-tooling/gitlab).
* ![ VMware Tanzu logo](/images/logos/TZLG-VMwareTanzu-icon.png) VMware Tanzu Kubernetes Grid (TKGI) validated 
    * GitLab is delivered to VMware based Kubernetes clusters via [helm chart](https://charts.gitlab.io/)
{:.list-capabilities}

***

## GitLab + VMware Tanzu Joint Solutions in Action
{:.no-color}

* [GitLab Tanzu Services Marketplace Listing](https://tanzu.vmware.com/solutions-hub/devops-tooling/gitlab)
* [VMworld-2020-demo/spring-music demo](https://gitlab.com/gitlab-com/alliances/vmware/sandbox/vmworld-2020-demo/spring-music)
* [GitLab to Enable Cloud Native Transformation on VMware Cloud Marketplace](https://www.globenewswire.com/news-release/2019/09/17/1916738/0/en/GitLab-to-Enable-Cloud-Native-Transformation-on-VMware-Cloud-Marketplace.html)
* [Webinar - Cloud Native](https://tanzu.vmware.com/content/webinars/jul-30-best-practices-for-cloud-native-pipelines-with-gitlab-and-vmware-tanzu?utm_campaign=Global_BT_Q221_Best-Practices-Cloud-Native-Pipeline-Gitlab&utm_source=sales-email&utm_medium=partner)
{:.list-resources}

***
